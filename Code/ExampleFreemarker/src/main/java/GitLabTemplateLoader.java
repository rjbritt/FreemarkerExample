import freemarker.cache.URLTemplateLoader;

import java.io.File;
import java.io.IOException;
import java.net.URL;

/**
 * @author ryanbritt on 6/8/16.
 */
public class GitLabTemplateLoader extends URLTemplateLoader {
    public GitLabTemplateLoader(GitLabApiHelper gitLabApiHelper) {
        this.gitLabApiHelper = gitLabApiHelper;
    }

    private GitLabApiHelper gitLabApiHelper;


    @Override
    /**
     * REALLY bad code. Just for example purposes only.
     *
     * Hard coded to one file,
     * does not resolve dependencies,
     * and it also creates a local file
     * for the file retrieved.
     */
    protected URL getURL(String name) {
        try {

            setURLConnectionUsesCaches(false);
            File temp = gitLabApiHelper.getFileForName(name);

            //TODO: deep copy the url info to be able to delete the temp file.
            URL finalUrl = temp.toURI().toURL();

            return finalUrl;

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
