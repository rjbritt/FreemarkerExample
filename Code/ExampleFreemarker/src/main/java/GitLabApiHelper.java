import org.apache.commons.codec.binary.Base64;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonString;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * @author ryanbritt on 6/10/16.
 */
public class GitLabApiHelper {

    private final String ENDPOINT = "https://gitlab.com/api/v3/projects/";

    private String projectId;
    private String privateToken;
    private String rootDirectory;
    private String branch;

    public GitLabApiHelper(String projectId, String privateToken, String rootDirectory, String branch) {
        this.projectId = projectId;
        this.privateToken = privateToken;
        this.rootDirectory = rootDirectory;
        this.branch = branch;
    }


    public String getUrlPathForName(String name) {
        return ENDPOINT +
                projectId +
                "/repository/files?private_token=" +
                privateToken +
                "&file_path=" +
                rootDirectory + "/" +
                name +
                "&ref=" + branch;
    }

    public byte [] getByteArrayForFileNamed(String name) {
        URL path = null;
        try {
            path = new URL(getUrlPathForName(name));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        try (InputStream is = path.openStream();
             JsonReader reader = Json.createReader(is)) {

            JsonObject rootObject = reader.readObject();
            JsonString contentObject = rootObject.getJsonString("content");
            return Base64.decodeBase64(contentObject.toString());

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
    public File getFileForName(String name) {
            byte [] fileBytes = getByteArrayForFileNamed(name);
            File temp = new File("tempFile");

        try {
            FileOutputStream fileOutputStream = new FileOutputStream(temp);
            fileOutputStream.write(fileBytes);
            fileOutputStream.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return temp;
    }
}
