import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorker;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.itextpdf.tool.xml.css.CssFile;
import com.itextpdf.tool.xml.css.StyleAttrCSSResolver;
import com.itextpdf.tool.xml.html.Tags;
import com.itextpdf.tool.xml.parser.XMLParser;
import com.itextpdf.tool.xml.pipeline.css.CSSResolver;
import com.itextpdf.tool.xml.pipeline.css.CssResolverPipeline;
import com.itextpdf.tool.xml.pipeline.end.PdfWriterPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipelineContext;
import freemarker.cache.FileTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

/**
 * @author ryanbritt on 6/6/16.
 */
public class Main {

    private static boolean useLocalRepo = true;
    private static String localRepositoryBaseDirectory = "/Users/ryanbritt/Developer/SPARC/FreemarkerExample/Templates";
    private static String CSS_LOCATION =
            "/Users/ryanbritt/Developer/SPARC/FreemarkerExample/Templates/Stylesheets/style.css";
    private static GitLabApiHelper apiHelper = new GitLabApiHelper("1274432", "zfHNz6kn-w78sJPWuPQE", "Templates", "master");

    //Useful for testing purposes
    private static String finalPdfOutput = "/Users/ryanbritt/Desktop/test2.pdf";

    public static void main(String[] args) throws Exception {

        /* ------------------------------------------------------------------------ */
        /* You should do this ONLY ONCE in the whole application life-cycle:        */

        /* Create and adjust the configuration singleton */
        Configuration cfg = new Configuration(Configuration.VERSION_2_3_24);
        cfg.setDefaultEncoding("UTF-8");
        cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
        cfg.setLogTemplateExceptions(false);
        cfg.setLocalizedLookup(false);
        cfg.clearTemplateCache();

        //Comment out the below line to access local template loader
        if (useLocalRepo) {
            cfg.setTemplateLoader(new FileTemplateLoader(new File(localRepositoryBaseDirectory)));
        } else {
            //You will need to get the private token from me
            cfg.setTemplateLoader(new GitLabTemplateLoader(apiHelper));
        }
        /* ------------------------------------------------------------------------ */

        /* Create and Merge data-model with template */
        Map dataModel = createDataModel();
        ByteArrayOutputStream outputStream = getHtmlOutputStream(dataModel, cfg, "test.ftlh");

        //Prepare for iText
        outputStream.close();
        ByteArrayInputStream htmlInputStream = new ByteArrayInputStream(outputStream.toByteArray());
        Document document = new Document();

        ByteArrayOutputStream pdfOutputStream = new ByteArrayOutputStream();
        PdfWriter writer = PdfWriter.getInstance(document, pdfOutputStream);
        document.open();

        if (useLocalRepo) {
            //Use a singular CSS file
            XMLWorkerHelper.getInstance().parseXHtml(writer, document, htmlInputStream, new FileInputStream(CSS_LOCATION));
        } else {
            //making the pdf from gitlab includes multiple css files just for learning.
            makePdfFromGitlab(document, writer, htmlInputStream);
        }

        document.close();
        writeOutputStreamToFile(pdfOutputStream);
    }

    private static void writeOutputStreamToFile(ByteArrayOutputStream outputStream) throws IOException {
        File newFile = new File(finalPdfOutput);

        if (!newFile.exists()) {
            newFile.createNewFile();
        }

        FileOutputStream fileOutputStream = new FileOutputStream(newFile);

        fileOutputStream.write(outputStream.toByteArray());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    private static ByteArrayOutputStream getHtmlOutputStream(Map dataModel, Configuration cfg, String templateName)
            throws IOException, TemplateException {

        /* Get the template (uses cache internally) */
        Template temp = cfg.getTemplate(templateName);

        /* Merge data-model with template */
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        Writer out = new OutputStreamWriter(outputStream);
        temp.process(dataModel, out);
        return outputStream;
    }

    private static Map createDataModel() {

        /* Create a data-model */
        Map root = new HashMap();
        root.put("user", "User");

        Product product1 = new Product();
        product1.setUrl("products/greenmouse.html");
        product1.setName("green mouse");

        Product product2 = new Product();
        product2.setUrl("products/bluemouse.html");
        product2.setName("blue mouse");

        Product product3 = new Product();
        product3.setUrl("products/graymouse.html");
        product3.setName("gray mouse");

        Product product4 = new Product();
        product4.setUrl("products/redmouse.html");
        product4.setName("red mouse");

        Product[] products = {product1, product2, product3, product4};
        root.put("products", products);

        return root;
    }

    private static void makePdfFromGitlab(Document document, PdfWriter writer, InputStream htmlInputStream) throws IOException {
        /*
        Alternate way of making a pdf on the document from multiple css files
         */
        // CSS - Default CSS as well as our custom one
        CSSResolver cssResolver = new StyleAttrCSSResolver();
        CssFile cssFile = XMLWorkerHelper.getCSS(
                new ByteArrayInputStream(apiHelper.getByteArrayForFileNamed("/Stylesheets/style.css")));
        cssResolver.addCss(XMLWorkerHelper.getInstance().getDefaultCSS());
        cssResolver.addCss(cssFile);

        // HTML
        HtmlPipelineContext htmlContext = new HtmlPipelineContext(null);
        htmlContext.setTagFactory(Tags.getHtmlTagProcessorFactory());

        // Pipelines
        PdfWriterPipeline pdf = new PdfWriterPipeline(document, writer);
        HtmlPipeline html = new HtmlPipeline(htmlContext, pdf);
        CssResolverPipeline css = new CssResolverPipeline(cssResolver, html);

        // XML Worker
        XMLWorker worker = new XMLWorker(css, true);
        XMLParser p = new XMLParser(worker);
        p.parse(htmlInputStream);
    }
}
